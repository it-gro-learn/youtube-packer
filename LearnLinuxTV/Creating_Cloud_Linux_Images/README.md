# Packer
* Creating Cloud Linux Images for Linode with Packer by Hashicorp
  * https://www.youtube.com/watch?v=wpEu2iFu7oI
  * [screenshots](screenshots)
  * https://wiki.learnlinux.tv/index.php/Building_Cloud_images_for_Linode_with_Packer
* https://www.packer.io/downloads

```
curl -LO https://releases.hashicorp.com/packer/1.6.6/packer_1.6.6_linux_amd64.zip
unzip packer_*.zip
rm packer_*.zip

sudo chown root:root packer
sudo mv packer /usr/local/bin/
```

```
mkdir packer_wd
cd packer_wd

nano linode-example.json
nano variables.json
```

```
{
  "variables": {
    "linode_api_token": ""
  },


  "builders": [{
    "type": "linode",
    "linode_token": "{{user `linode api token`}}",
    "image": "linode/debian10",
    "region": "us-east",
    "instance_type": "g6-nanode-1",
    "ssh_username": "root",
    "instance_label": "myserver-{{timestamp}}",
    "image_label": "my-image-{{timestamp}}",
    "image_description": "My first Packer image"
  }]
}
```

```
packer
packer build -var-file=variables.json linode-example.json
```


```
{
  "variables": {
    "linode_api_token": ""
  },

  "builders": [{
    "type": "linode",
    "linode_token": "{{user `linode api token`}}",
    "image": "linode/debian10",
    "region": "us-east",
    "instance_type": "g6-nanode-1",
    "ssh_username": "root",
    "instance_label": "myserver-{{timestamp}}",
    "image_label": "my-image-{{timestamp}}",
    "image_description": "My first Packer image"
  }],

  "provisioners": [{
    "type": "shell",
    "script": "script.sh"
  }]

}
```


`script.sh`

```
#!/bin/bash

sudo apt update
sudo apt install -y apache2
```

```
packer build -var-file=variables.json linode-example-2.json
```
